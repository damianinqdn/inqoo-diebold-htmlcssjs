```js
productNameInput.onkeyup = e => console.log(e.target.value);

console.log(1)

// queue Microtask
Promise.resolve(2).then(console.log)

function echo(data, cb) {  cb(data) }
echo(3, console.log)

function myAsyncEcho(data, cb){
    // queue Macrotask
    setTimeout(function handleResult(){ cb(data) },0)
}
myAsyncEcho(4, console.log )

start = Date.now() 
while( start + 5000 > Date.now()){ } // block

console.log(5)



1 // sync console.log
3 // sync callback 
5 // sync console.log
undefined // sync code finished / returned
2 // Promise resolved - microtask 
123 // user input - macrotask
1234
12345
4 // setTimeout - Macrotask queue

```

```js
function echo(msg, cb){
    setTimeout(()=>{
        cb(msg)
    }, 2000)
};

echo('Ala ', (msg) => {
    echo(msg + ' ma', msg => {
        echo(msg + ' kota', res => console.log(res));
    });
});


/// 6 sec...
Ala  ma kota
```

## Error handling
```js

try{
    echo('test', x => {
        x.y.z;
    }) 


}catch(e){
//     console.error('Unexpected error')
    x.y = {z: 'Default z'}
}
x 
```


## Async - callback + error 
```js
function echo(msg, cb){
    setTimeout(()=>{
        cb(error, msg)
    }, 2000)
};

echo('Ala ', (error,msg) => {
    if(error){ return console.log(error) }
    
    echo(msg + ' ma', (error,msg) => {
        if(error){ return console.log(error)}
        
        echo(msg + ' kota', (error,msg) => {
            if(error){ return console.log(error)}
            
            console.log(res)
        });
    });
});


``` 