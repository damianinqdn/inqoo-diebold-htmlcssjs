
let product = new Product();
let filter = new Filter();
let maxPrice = 100;
let filteredProducts = filter.filterMaxPrice(maxPrice);


document.addEventListener("DOMContentLoaded", function(){
    displayList();
});


function displayList() {
    // filter.filterMaxPrice(maxPrice);
    document.getElementById('list-group-js').innerHTML = '';
    filteredProducts.forEach(p => {
        let printItem = prepareProductToDisplay(p);
        document.getElementById('list-group-js').innerHTML += printItem;
    });
}
