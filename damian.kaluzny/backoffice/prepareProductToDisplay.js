function prepareProductToDisplay(product) {
    return `<li class="list-group-item list-group-item-action row">
                <table>
                    <tr class="d-flex justify-content-between">
                        <th scope="col">` + product.name + `</th>
                        <th scope="col">` + product.price + `</th>
                    </tr>
                    <tr class="d-flex justify-content-between">
                        <td>` + product.describe + `</td>
                        <th><button class="btn btn-success justify-content-end" onclick="add(` + product.id + `)">
                            Add to cart</button>
                        </th>
                    </tr>
                </table>
            </li>`;
}
