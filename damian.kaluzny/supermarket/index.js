console.log("Hello world");

var product = {
id: 123,
name : "Banana Pencakes",
price : 16.99,
describe : "Describe describes described product",
promotion : true,
tax : 23,
image : "https://unsplash.com/photos/cPF2nlWcMY4",
date_added : new Date(),
}

var gross_price = product.price + product.price * product.tax / 100;

/* Kopia obiektu */
/* asign przypisje wartości z product do product2 */
var product2 = Object.assign({}, product);
product2.name = "Cookies";
product2.id = 1234567;
product2.price = 1420;

/* ... */
var product3 = {
    ...product2,
    rating: {...product2.rating},
    id: '123',
    name: "Product 3",
}

/* Szybki sposób kopiowania pól (pomija metody) */
var product4 = JSON.parse(JSON.stringify(product2));
product3.id = '133';
product3.name = "Nazwa 4";


/* Szybkie przełączanie między obiektami */


var parsenumber = parseInt("100", 2);
var parsenumber = parseInt("123", 10);


console.log(product.id +" \n"+ product.name +" \n"+ product.price +" \n"+ product.describe +" \n"+ product.tax +" \n"+ product.date_added +" \n"+ product.image +"\n Brutto: \n"+ gross_price.toPrecision(4));
console.log("Promotion: " + product.promotion ?? "");
console.log("Product 1 name: "+ product.name);
console.log("Product 2 name: "+ product2.name);
console.log("Product 2 price: "+ product2.price);