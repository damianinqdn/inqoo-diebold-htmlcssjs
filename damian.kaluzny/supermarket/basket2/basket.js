// discount for promotioned products 10%
var shop_discount = 0.1;
// sum of basket products
var sum = 0;

var products = [
    {
        id: 101,
        name : "Banana Pencakes",
        price : 16.99,
        describe : "Beast Banana Pencakes in the world!",
        promotion : true,
        tax : 23,
        image : "https://unsplash.com/photos/cPF2nlWcMY4",
        date_added : new Date(),
        basket_placed : false,
    },
    {
        id: 102,
        name : "Strawberry Cookies",
        price : 26.19,
        describe : "These Strawberry Cookies are my favorite Strawberry Cake Mix Cookies!",
        promotion : true,
        tax : 23,
        image : "https://unsplash.com/photos/cPF2nlWcMY4",
        date_added : new Date(),
        basket_placed : false,
    },
    {
        id: 103,
        name : "Ice cream",
        price : 6.99,
        describe : "Chocolate ice cream is a flavour of ice cream which is the second most common flavour of ice cream, after vanilla.",
        promotion : false,
        tax : 23,
        image : "https://unsplash.com/photos/cPF2nlWcMY4",
        date_added : new Date(),
        basket_placed : false,
    }
];

// basket items
var basketItems = [{
    product_id : 101,
    amount : 1,
    subtotal : 1,
    product : products[0]
}]

document.addEventListener("DOMContentLoaded", function(){
    displayProductList();
});

/** Find product in basket by id */
function findByIdInBasket(id) {
    basketItems.forEach(item => {
        if(item.product_id === id) return item;
    });
}

/** Find product in store list */
function findByIdInStore(id) {
    products.forEach(item => {
        if(item.id === id) return item;
    })
}

/** Generate html of all products as list elements */
function displayProductList() {
    products.forEach(product => {
        let printItem = prepareProductList(product);
        document.getElementById('list-group-js').innerHTML += printItem;
    });
}

/** Add product into basket list */
function add(id) {
    if(findByIdInBasket(id).product_id == id)
    basketItems.forEach(item => {
        if(item.product_id == id && !item[0].basket_placed) {
            item[0].basket_placed = true;
            let printItem = prepareBasketList(item[0]);
            document.getElementById("basket").innerHTML += printItem;
            addToSum(item[0].price);
        }
        else if(item[0].id == id && item[0].basket_placed) {
            item.amount += 1;
        }
    });
    updateSum();
}
/** Display current total */
function updateSum() {
    document.getElementById("basket-sum").innerHTML = sum.toFixed(2);
}

/** Increase sum */
function addToSum(price) {
    sum += price;
    updateSum();
}

/** Decrease sum */
function subtractFromSum(price) {
    sum -= price;
    if(sum < 0) sum = 0;
    updateSum();
}

/** Remove product from basket */
function remove(id) {
    products.forEach(product => {
        if(id == product.id) {
            product.basket_placed = false;
            document.getElementById("basket"+id).remove();
            subtractFromSum(product.price);
        }
    });
}
/** ################################# */
/** Preparing html elements */
function prepareBasketList(product) {
    return `<li class="list-group-item list-group-item-action row" id="basket` + product.id + `">
                <table>
                    <tr class="d-flex justify-content-between">
                        <td scope="col">` + product.name + `</td>
                        <td scope="col">` + product.price + `</td>
                    </tr>
                    <tr>
                        <th scope="col">
                            <button class="btn btn-danger justify-content-end" onclick="remove(` + product.id + `)">
                                Remove</button>
                        </th>
                    </tr>
                </table>
            </li>`;
}

function prepareProductList(product) {
    return `<li class="list-group-item list-group-item-action row">
                <table>
                    <tr class="d-flex justify-content-between">
                        <th scope="col">` + product.name + `</th>
                        <th scope="col">` + product.price + `</th>
                    </tr>
                    <tr class="d-flex justify-content-between">
                        <td>` + product.describe + `</td>
                        <th><button class="btn btn-success justify-content-end" onclick="add(` + product.id + `)">
                            Add to cart</button>
                        </th>
                    </tr>
                </table>
            </li>`;
}