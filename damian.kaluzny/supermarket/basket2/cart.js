/** Read all html products */
var items = document.querySelectorAll("products-js");

var cartItems = [
    {
        product_id = '102',
        product: products[0],
        amount: 2,
        subtotal: 20
    }
]

var cartTotal = 20;

// Automated test
addToCart(111);
addToCart(112);
addToCart(113);

function addToCart(product_id) {
    console.log("addToCart");
    // ADD product to Cart
    cartItems.forEach(function (item) {
        // increse amount if product exist in the basket
        if(item.product_id == product_id) {
            item.amount += 1;
        }
        // add product if not exist in the basket
        else {
            cartItems.push(item);
        }
    })
}

/** Find product by Id
 * @params number
 * @returns (Object) item
 */
function getProductById(product_id) {
    items.forEach(item => {
        if(item.id == product_id) {
            return item;
        }
    })
}

console.log(cartItems);