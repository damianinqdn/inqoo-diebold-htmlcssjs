var tax = 23
// Apply to PROMOTED products that don`t have their own discount
var shop_discount = 0.5;

var products = [
  {
    id: '123',
    name: "Banana Pancakes",
    price: 1410,
    description: "Best pancakes ever",
    promotion: false,
    discount: 0.1,
    image: "https://via.placeholder.com/80",
    date_added: new Date(),
    category: ['pancakes'],
  },
  {
    id: '123',
    name: "Strawbery Pancakes",
    price: 2344,
    description: "Ineresting pancakes",
    promotion: true,
    image: "https://via.placeholder.com/80",
    date_added: new Date(),
    category: ['pancakes'],
    rating: { votes: 123, rate: 4.5 }
  },
  {
    id: '34',
    name: "Banana cookies",
    price: 1243,
    description: "Awesome cookies",
    promotion: true,
    discount: 0.00,
    image: "https://via.placeholder.com/80",
    date_added: new Date(2021, 10, 1),
    category: ['cookies'],
    rating: { votes: 2343, rate: 3.5 }
  },
]

// debugger

var filter_promoted = true;
var limit = 2
var count = 0;
var order = 'asc'
// var order = 'desc'

products = order === 'asc'? products : products.reverse()

for (let product of products) {
  if (checkCredentials(product)) continue;
  if (count++ > limit) break;
  /* == Rendering === */
  var description = getProductDesc(product);
  /* == DISCOUNTS === */
  var nett_price = calculateDiscounts(product);
  /* == TAXES ==  */
  var gross_price = calcTax();
  /* == PRODUCT INFO DISPLAY == */
  var productInfo = displayProduct(product);
  // Banana Pancakes - Best pancakes ever - 16.90 PROMO 31-08-2021
  print();
  /* Sum of products */
  var sum = calcSum(product);
  console.log("Suma: "+ sum);
}

/** Sum of all products
 * @param (Object) Product
 * @returns number */ 
function calcSum(product) {
  var sum = 0;
  sum += product.price;
  return sum;
}

/** Print on web page */
function print() {
  console.log(productInfo);
  var printItem = '<h1>Placki</h1>';
  /* var box = document.getElementById("list-group"); */
  box.innerHTML += printItem;
  console.log(printItem);
}

/** Check promotions for products
 * @param (Object) product
 * @returns bool */
function checkCredentials(product) {
  return filter_promoted == true && product.promotion == false;
}
/** Prepare product to display
 * @param {Object} product
 * @returns String */
function displayProduct(product) {
  return count + '. ' + product.name + ' ' + description + ' ' + gross_price.toFixed(2) + (product.promotion ? 'PROMOTION' : '') + product.date_added;
}

/** Calculate Tax Value
 * @param {number} nett_price
 * @returns number */
function calcTax() {
  return Math.round(nett_price * (1 + tax / 100)) / 100;
}

/** Dicsouts calculate for product 
 * @params {Object}
 * @returns number */
function calculateDiscounts(product) {
  var discount = 'number' === typeof product.discount ? product.discount : shop_discount;
  var nett_price = product.price * product.promotion ? discount : 0;
  return nett_price;
}

/** Rendering 
 * @param (Object) Product
 * @returns String */  
function getProductDesc(product) {
  var description = product.description;
  switch (product.category[0]) {
    case 'pancakes':
      description += ' Amerykanskie '; /* + 'Świetne nalesniki '; break; */
    case 'nalesnik':
      description += ' Świetne nalesniki '; break;
    case 'ciastka': description += ' Pyszne ciastka '; break;
    default: description += ' Inne słodkości';
  }
  return description;
}


